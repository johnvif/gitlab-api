/*
 * Copyright (C) 2018, 2019 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {asyncPool} from '@krlwlfrt/async-pool';
import {Logger} from '@openstapps/logger';
import {AddLogLevel} from '@openstapps/logger/lib/transformations/add-log-level';
import {Colorize} from '@openstapps/logger/lib/transformations/colorize';
import {Command} from 'commander';
import {readFileSync} from 'fs';
import {join} from 'path';
import {Api, ApiRequestOptions} from './api';
import {Issue} from './types';

Logger.setTransformations([
  new AddLogLevel(),
  new Colorize(),
]);

const pkgJson = JSON.parse(readFileSync(join(__dirname, '..', 'package.json'))
  .toString());

const commander = new Command('openstapps-gitlab-api');

commander
  .version(pkgJson.version);

commander
  .option('-t, --token [token]', 'GitLab API token', process.env.GITLAB_PRIVATE_TOKEN)
  .option('-u, --url [url]', 'GitLab API URL', 'https://gitlab.com/api/v4/');

commander
  .command('request <call> [method] [data]')
  .action(async (call, method, data) => {
    const options: ApiRequestOptions = {};

    if (method !== 'GET') {
      options.method = method;
    }

    if (typeof data !== 'undefined') {
      options.data = JSON.parse(data);
    }

    const api = new Api(commander.url, commander.token);

    const result = await api.makeGitLabAPIRequest(call, options);

    // tslint:disable-next-line:no-console
    console.log(result);
  });

commander
  .command('batch-process <projectId> <action>')
  .action(async (projectId, action) => {
    if (!['close'].includes(action)) {
      await Logger.error('Only "close" is supported as action.');
    }

    const api = new Api(commander.url, commander.token);

    const issues = await api.makeGitLabAPIRequest(`/projects/${projectId}/issues?state=opened`, {
      retryOnAnyError: true,
      tries: 10,
    }) as Issue[];

    Logger.log(`Fetched ${issues.length} issue(s).`);

    // tslint:disable-next-line:no-magic-numbers
    await asyncPool(5, issues, async (issue) => {
      if (action === 'close') {
        Logger.info(`Closing issue #${issue.iid} of project '${projectId}': ${issue.title}.`);

        await api.makeGitLabAPIRequest(`/projects/${projectId}/issues/${issue.iid}`, {
          data: {
            state_event: 'close',
          },
          method: 'PUT',
          retryOnAnyError: true,
          tries: 10,
        });
      }
      // tslint:disable-next-line:no-console
      Logger.info(`Processed issue #${issue.iid} of project '${projectId}': ${issue.title}`);
    });

    Logger.ok('Processed all issues.');
  });

commander
  .parse(process.argv);

if (typeof commander.token !== 'string' || commander.token.length === 0) {
  Logger.warn('You probably want to supply a GitLab token either via option or environment variable (GITLAB_PRIVATE_TOKEN).');
}
